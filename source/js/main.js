'use strict';

(function () {

  class ListItemsPopup {
    constructor() {
      this.elements = document.querySelectorAll('.content-block__list-item');
      this.isOpen = false;
    }

    show (element) {
      if (element.children.length === 0) {
        return;
      }

      let subListElement = element.querySelector('.content-block__sublist');
      subListElement.classList.remove('visually-hidden');
      subListElement.classList.add('show-element');

      document.addEventListener('keydown', this.onEscape);

      this.isOpen = true;
    }

    hide (element) {
      if (element.children.length === 0) {
        return;
      }

      let subListElement = element.querySelector('.content-block__sublist');
      subListElement.classList.add('visually-hidden');
      subListElement.classList.remove('show-element');

      document.removeEventListener('keydown', this.onEscape);

      this.isOpen = false;
    }

    onEscape (ev) {
      listItemsPopup.elements.forEach(function (element) {

        if (ev.key === 'Escape') {
          ev.preventDefault();

          listItemsPopup.hide(element);
        }
      })
    }

    bindEvents () {
      let $this = this;

      this.elements.forEach(function (element) {
        element.addEventListener('mouseover', function (ev) {
          ev.stopPropagation();

          if (!$this.isOpen) {
            $this.show(element);
          }
        });

        element.addEventListener('focus', function (ev) {

          if (!$this.isOpen) {
            $this.show(element);
          }
        });

        element.addEventListener('mouseleave', function (ev) {

          if ($this.isOpen) {
            $this.hide(element);
          }
        });

        element.addEventListener('focusout', function (ev) {

          if ($this.isOpen) {
            $this.hide(element);
          }
        });

      });

    }

  }

  let listItemsPopup = new ListItemsPopup();
  listItemsPopup.bindEvents();

})();
